from django.shortcuts import render
from django.views.generic import View
from django.views.generic.edit import CreateView
from django.urls import reverse, reverse_lazy
from .models import versiones_python, mensaje
from .forms import mensajeForm
# Create your views here.

class index(View):
    def get(self, request, *args, **kwargs):
        context = {

        }
        return render(request, "core/index.html", context)

class acerca_de(View):
    def get(self, request, *args, **kwargs):
        list_vers_python = versiones_python.objects.all()
        context = {
            'listado_versiones': list_vers_python
        }
        return render(request, "core/acercade.html", context)

class raul(View):
    def get(self, request, *args, **kwargs):
        context = {

        }
        return render(request, "core/raul.html", context)   

class martin(View):
    def get(self, request, *args, **kwargs):
        context = {

        }
        return render(request, 'core/portfolio_martin.html', context)

class contactanos(CreateView):
    model = mensaje
    form_class = mensajeForm
    success_url = reverse_lazy('core:acerca_de')