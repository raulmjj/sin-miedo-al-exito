from django.urls import path
from . import views

app_name="core"

urlpatterns = [
    path("", views.index.as_view(), name='index'),
    path("acerca_de/", views.acerca_de.as_view(), name='acerca_de'),
    path("raul/", views.raul.as_view(), name='portafolio_raul'),
    path("martin/", views.martin.as_view(), name='portafolio_martin'),
    path("contactanos/", views.contactanos.as_view(), name='contactanos'),
]