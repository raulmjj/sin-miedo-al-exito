from django.db import models
from django.urls import reverse
# Create your models here.

class versiones_python(models.Model):
    nombre = models.CharField(max_length=80, verbose_name="Nombre")
    descripcion = models.TextField(verbose_name="Descripcion")
    imagenes = models.ImageField(verbose_name="Imagen", upload_to="imagenes_versiones")
    url = models.URLField(verbose_name="Link Referencia", max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

class Meta:
    verbose_name = "Version"
    verbose_name_plural = "Versiones"
    ordering = ["created"]


def __str__(self):
    return self.nombre

class mensaje(models.Model):
    nombre = models.CharField(max_length=75, verbose_name="Nombre")
    apellido = models.CharField(max_length=75, verbose_name="Apellido")
    domicilio = models.CharField(max_length=100, verbose_name="Domicilio")
    ciudad = models.CharField(max_length=75, verbose_name="Ciudad")
    pais = models.CharField(max_length=10, verbose_name="PaÍs")
    codigo_postal = models.CharField(max_length=50, verbose_name="Codigo Postal")
    email= models.EmailField(max_length=150, verbose_name="Email")
    num_telefono = models.CharField(max_length=75, verbose_name="Numero de Telefono")
    msj = models.TextField(verbose_name="Mensaje")

    def get_absolute_url(self):
        return reverse('curso_detail')

    class Meta:
        verbose_name = "Mensaje"
        verbose_name_plural = "Mensajes"
    
    def __str__(self):
        return self.nombre