from django import forms
from .models import mensaje

class mensajeForm(forms.ModelForm):

    class Meta:
        model = mensaje
        fields = [
                    'nombre',
                    'apellido',
                    'domicilio',
                    'ciudad',
                    'pais',
                    'codigo_postal',
                    'email',
                    'num_telefono',
                    'msj'
                ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control','placeholder':'Título'}),
            'apellido': forms.TextInput(attrs={'class':'form-control','placeholder':'Apellido'}),
            'domicilio': forms.TextInput(attrs={'class':'form-control','placeholder':'Domicilio'}),
            'ciudad': forms.TextInput(attrs={'class':'form-control','placeholder':'Domicilio'}),
            'codigo_postal': forms.TextInput(attrs={'class':'form-control','placeholder':'Codigo Postal'}),
            'email': forms.EmailInput(attrs={'class':'form-control','placeholder':'Email'}),
            'num_telefono': forms.TextInput(attrs={'class':'for-control','placeholder':'Numero de Telefono'}),
            'msj': forms.Textarea(attrs={'class':'form-control','placeholder':'mensaje'})
        }

        labels = {
                    'nombre':'Nombre:',
                    'apellido':'Apellido:',
                    'domicilio':'Domicilio:',
                    'ciudad':'Ciudad:',
                    'pais':'País:',
                    'codigo_postal':'Codigo Postal:',
                    'email':'Email:',
                    'num_telefono':'Número de Telefono (Celular)',
                    'msj':'Mensaje'
            }