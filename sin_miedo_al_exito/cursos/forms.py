from django import forms
from .models import curso

class cursoForm(forms.ModelForm):

    class Meta:
        model = curso
        fields = [
                    'nombre',
                    'desc_corta',
                    'desc_larga',
                    'imagen',
                    'url_video',
                    'url_documentacion'
                ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control','placeholder':'Título'}),
            'desc_corta': forms.Textarea(attrs={'class':'form-control','placeholder':'Descripcion corta'}),
            'desc_larga': forms.Textarea(attrs={'class':'form-control','placeholder':'Descripcion larga'}),
            'imagen': forms.FileInput(attrs={'class':'form-control','placeholder':'Imagen'}),
            'url_video': forms.URLInput(attrs={'class':'form-control','placeholder':'Link Video'}),
            'url_descripcion': forms.URLInput(attrs={'class':'form-control','placeholder':'Link Documentacion'})
        }

        labels = {
                    'nombre': 'Nombre',
                    'desc_corta': 'Descripción corta',
                    'desc_larga': 'Descripción larga',
                    'imagen': 'Imagen',
                    'url_video': 'Video',
                    'url_documentacion': 'Documentación'
            }