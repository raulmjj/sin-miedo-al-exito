from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views

app_name="cursos"

urlpatterns = [
    path('listado_de_cursos/',views.cursoListView.as_view(), name='listado_cursos' ),
    path('<int:pk>/', views.cursoDetailView.as_view(), name='curso_detalle'),
    path('crear_curso/', login_required(views.cursoCreate.as_view()), name='curso_crear'),
    path('actualizar_curso/<int:pk>/', login_required(views.cursoUpdate.as_view()), name='actualizar_curso'),
    path('borrar_curso/<int:pk>/', login_required(views.cursoDelete.as_view()), name='borrar_curso'),
]