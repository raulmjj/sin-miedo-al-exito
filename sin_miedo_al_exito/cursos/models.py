from django.db import models
from django.urls import reverse
# Create your models here.

class curso(models.Model):
    nombre=models.CharField(max_length=75, verbose_name="Nombre")
    desc_corta=models.TextField(verbose_name="Descripcion_Corta", max_length=400)
    desc_larga=models.TextField(verbose_name="Descripción_larga")
    imagen=models.ImageField(verbose_name="Imagen", upload_to="imagenes_curso")
    url_video=models.URLField(verbose_name="link_video" )
    url_documentacion=models.URLField(verbose_name="link_documentacion")
    created=models.DateTimeField(auto_now_add=True)
    updated=models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('curso_detail', kwargs={'pk': self.pk})

    class Meta:
        verbose_name = "Curso"
        verbose_name_plural = "Cursos"
        ordering = ["created"]

    def __str__(self):
        return self.nombre