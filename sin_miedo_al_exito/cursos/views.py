from django.shortcuts import render, get_object_or_404
from .models import curso
from django.views.generic.detail import DetailView
from django.db.models import Q
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from cursos.models import curso
from django.urls import reverse, reverse_lazy
from .forms import cursoForm
from django.core.paginator import Paginator
# Create your views here.
class cursoListView(ListView):

    def get(self, request, *args, **kwargs):
        busqueda = request.GET.get("buscar")
        cursos_lista = curso.objects.all()
        if busqueda:
            cursos_lista = curso.objects.filter(
                Q(nombre__icontains = busqueda)
            ).distinct()
        page_numero = request.GET.get('page')
        cursos_page = Paginator(cursos_lista, 3)
        cursos_lista = cursos_page.get_page(page_numero)
        context = {
            'curso_list': cursos_lista,
            'busqueda': busqueda,
        }
        return render(request, "cursos/curso_list.html", context)
    model = curso

class cursoDetailView(DetailView):
    def get(self, request, *args, **kwargs):
        curso_de = get_object_or_404(curso, pk=kwargs['pk'])
        context = {'curso': curso_de}
        return render(request, 'cursos/curso_detail.html', context)

    model = curso


class cursoCreate(CreateView):
    model = curso
    form_class = cursoForm
    success_url = reverse_lazy('cursos:listado_cursos')

class cursoUpdate(UpdateView):
    model = curso
    form_class = cursoForm
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse_lazy('cursos:listado_cursos')+'?Actualizado'

class cursoDelete(DeleteView):
    model = curso
    def get_success_url(self):
        return reverse_lazy('cursos:listado_cursos')+'?Eliminado'    