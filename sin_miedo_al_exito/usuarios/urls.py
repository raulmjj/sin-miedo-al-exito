from django.urls import path
from django.contrib.auth.views import LoginView, logout_then_login
from django.views.generic import TemplateView

app_name = "usuarios"

urlpatterns = [
    path('accounts/login/',LoginView.as_view(template_name= 'usuarios/login.html'), name='inicio_sesion'),
    path('logout/',logout_then_login, name='cerrar_sesion' )
]
